//
//  File.swift
//  
//
//  Created by Aunish Kewat on 29/12/21.
//

import Foundation
public struct Multiplication:Calculatorprotocol{
    public init() {}
    public func calculates(firstValue: Int, secondValue: Int) -> Int {
        return firstValue * secondValue
    }
}

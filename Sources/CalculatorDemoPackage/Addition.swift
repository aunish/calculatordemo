//
//  File 2.swift
//  
//
//  Created by Aunish Kewat on 29/12/21.
//

import Foundation
public struct Addition:Calculatorprotocol{
    public init() {}
    public func calculates(firstValue: Int, secondValue: Int) -> Int {
        return firstValue + secondValue
    }
}

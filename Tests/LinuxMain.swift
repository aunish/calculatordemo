import XCTest

import CalculatorDemoPackageTests

var tests = [XCTestCaseEntry]()
tests += CalculatorDemoPackageTests.allTests()
XCTMain(tests)
